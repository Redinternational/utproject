﻿
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	рзОбработкаОтветовЕГАИС = РегламентныеЗадания.НайтиПредопределенное(Метаданные.РегламентныеЗадания.ОбработкаОтветовЕГАИС);
	РасписаниеПроверкиОтветов = рзОбработкаОтветовЕГАИС.Расписание;
	ИспользоватьАвтоОбработкуОтветовЕГАИС = рзОбработкаОтветовЕГАИС.Использование;
	Элементы.НадписьОткрытьРасписание.Заголовок = ТекстНадписиОткрытьРасписание(РасписаниеПроверкиОтветов);
	Элементы.НадписьОткрытьРасписание.Доступность = ИспользоватьАвтоОбработкуОтветовЕГАИС;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ

&НаКлиенте
Процедура НадписьОткрытьРасписаниеНажатие(Элемент)
	
	Если РасписаниеПроверкиОтветов = Неопределено Тогда
		РасписаниеПроверкиОтветов = Новый РасписаниеРегламентногоЗадания;
	КонецЕсли;
	
	Диалог = Новый ДиалогРасписанияРегламентногоЗадания(РасписаниеПроверкиОтветов);
	
	Если Диалог.ОткрытьМодально() Тогда
		РасписаниеПроверкиОтветов = Диалог.Расписание;
		Элементы.НадписьОткрытьРасписание.Заголовок = ТекстНадписиОткрытьРасписание(РасписаниеПроверкиОтветов);
		ЗаписатьРасписаниеОбработкиОтветов(РасписаниеПроверкиОтветов);
	КонецЕсли;
	
КонецПроцедуры


////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

&НаКлиенте
Процедура НадписьОткрытьРасписаниеНажатиеЗавершение(Расписание, ДополнительныеПараметры) Экспорт
	
	Если Расписание <> Неопределено Тогда
		РасписаниеПроверкиОтветов = Расписание;
		Элементы.НадписьОткрытьРасписание.Заголовок = ТекстНадписиОткрытьРасписание(РасписаниеПроверкиОтветов);
		ЗаписатьРасписаниеОбработкиОтветов(РасписаниеПроверкиОтветов);
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ЗаписатьРасписаниеОбработкиОтветов(РасписаниеПроверкиОтветов)
	
	РегламентноеЗадание = РегламентныеЗадания.НайтиПредопределенное(Метаданные.РегламентныеЗадания.ОбработкаОтветовЕГАИС);
	РегламентноеЗадание.Расписание = РасписаниеПроверкиОтветов;
	РегламентноеЗадание.Записать();
	
	ОбновитьПовторноИспользуемыеЗначения();
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ТекстНадписиОткрытьРасписание(Расписание)
	
	СтроковоеПредставлениеРасписания = Строка(Расписание);
	Возврат ?(Не ПустаяСтрока(СтроковоеПредставлениеРасписания),
		СтроковоеПредставлениеРасписания, НСтр("ru = 'Не задано'"));
		
КонецФункции

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ИнтеграцияЕГАИСКлиентСерверПереопределяемый.ПроверитьИспользованиеПодсистемы(Отказ);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьАвтоОбработкуОтветовЕГАИСПриИзменении(Элемент)
	
	ИспользоватьАвтоОбработкуОтветовЕГАИСПриИзмененииНаСервере();
	
	Элементы.НадписьОткрытьРасписание.Доступность = ИспользоватьАвтоОбработкуОтветовЕГАИС;
	
КонецПроцедуры

&НаСервере
Процедура ИспользоватьАвтоОбработкуОтветовЕГАИСПриИзмененииНаСервере()
	
	РегламентноеЗадание = РегламентныеЗадания.НайтиПредопределенное(Метаданные.РегламентныеЗадания.ОбработкаОтветовЕГАИС);
	РегламентноеЗадание.Использование = ИспользоватьАвтоОбработкуОтветовЕГАИС;
	РегламентноеЗадание.Записать();
	
КонецПроцедуры
