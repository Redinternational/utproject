﻿Перем мИспользоватьТару;

// Возвращает наличие строк в табличных частях, которые предполагается заполнить
//
Функция ЕстьСтрокиВДокументе()

	Возврат ДокументОбъект.Товары.Количество() <> 0 И ФлажокТовары
	        ИЛИ НЕ НеЗаполнятьУслуги И ДокументОбъект.Услуги.Количество() <> 0 И ФлажокУслуги 
	        ИЛИ НЕ НеЗаполнятьТару И ДокументОбъект.ВозвратнаяТара.Количество() <> 0 И ФлажокТара;

КонецФункции // ЕстьСтрокиВДокументе()
 
// Устанавливает доступность элементов зависимости от режима заполнения.
//
Процедура УстановитьДоступностьЭлементовПанелиОтбораНоменклатуры()
	
	Если НеЗаполнятьТовары Тогда
		ЭлементыФормы.ФлажокТовары.Доступность = Ложь;
	Иначе
		ЭлементыФормы.ФлажокТовары.Доступность = Истина;
	КонецЕсли;
	
	Если НеЗаполнятьУслуги Тогда
		ЭлементыФормы.ФлажокУслуги.Доступность = Ложь;
	Иначе
		ЭлементыФормы.ФлажокУслуги.Доступность = Истина;
	КонецЕсли;
	
	Если мИспользоватьТару Тогда
		ЭлементыФормы.ФлажокТара.Видимость = Истина;
		Если НеЗаполнятьТару Тогда
			ЭлементыФормы.ФлажокТара.Доступность = Ложь;
		Иначе
			ЭлементыФормы.ФлажокТара.Доступность = Истина;
		КонецЕсли;
	Иначе
		ЭлементыФормы.ФлажокТара.Видимость = Ложь;
		НеЗаполнятьТару = Истина;
	КонецЕсли;
	
	
КонецПроцедуры

// Устанавливает доступность кнопок основных действий формы.
//
Процедура УстановитьДоступностьКнопок()

	Если ФлажокТовары ИЛИ ФлажокТара ИЛИ ФлажокУслуги Тогда
		ДоступностьКнопок = Истина;
	Иначе
		ДоступностьКнопок = Ложь;
	КонецЕсли;
	
	Если ЕстьСтрокиВДокументе() Тогда
		ЭлементыФормы.ОсновныеДействияФормы.Кнопки.ДействиеДобавить.Доступность = ДоступностьКнопок;
		ЭлементыФормы.ОсновныеДействияФормы.Кнопки.ДействиеДобавить.КнопкаПоУмолчанию = ДоступностьКнопок;
		
		ЭлементыФормы.ОсновныеДействияФормы.Кнопки.ДействиеЗаполнить.Текст = "Очистить и заполнить";
	Иначе
		ЭлементыФормы.ОсновныеДействияФормы.Кнопки.ДействиеДобавить.Доступность = Ложь;
		
		ЭлементыФормы.ОсновныеДействияФормы.Кнопки.ДействиеЗаполнить.Текст = "Заполнить";
	КонецЕсли;

	ЭлементыФормы.ОсновныеДействияФормы.Кнопки.ДействиеЗаполнить.Доступность = ДоступностьКнопок;
	
КонецПроцедуры

// Открывает форму отбора заказов.
//
Процедура ОткрытьФормуОтбораЗаказов()
	
	Если ДокументОбъект.Модифицированность() Тогда
		
		Попытка
			Если ДокументОбъект.Проведен Тогда
				РезультатЗаписи = ФормаДокумента.ЗаписатьВФорме(РежимЗаписиДокумента.Проведение);	
			Иначе
				РезультатЗаписи = ФормаДокумента.ЗаписатьВФорме(РежимЗаписиДокумента.Запись);	
			КонецЕсли; 
		Исключение
			РезультатЗаписи = Ложь;
		КонецПопытки;
		
		Если НЕ РезультатЗаписи Тогда
			Предупреждение("Невозможно записать документ.");
			Возврат;
		КонецЕсли; 
		
	КонецЕсли; 
	
	Если ДокументОбъект.ЭтоНовый() ИЛИ ДокументОбъект.Модифицированность() Тогда
		ЭтаФорма.Закрыть();
		Возврат
	КонецЕсли;
	
	// Получим форму.
	Форма = ДокументОбъект.ПолучитьФорму("ФормаОтбораЗаказов", ФормаДокумента, );

	//Установим реквизиты и переменные формы.
	Форма.ПоказыватьТовары       = ФлажокТовары;
	Форма.ПоказыватьУслуги       = ФлажокУслуги;
	Форма.ПоказыватьТару         = ФлажокТара;

	Форма.НеЗаполнятьТовары       = НеЗаполнятьТовары;
	Форма.НеЗаполнятьУслуги       = НеЗаполнятьУслуги;
	Форма.НеЗаполнятьТару         = НеЗаполнятьТару;
	
	Форма.ДокументОбъект = ДокументОбъект;
	
	ЭтаФорма.Закрыть();

	Форма.Открыть();
КонецПроцедуры

// Процедура обработчик события Нажатие кнопки "Очистить и заполнить".
//
Процедура ОсновныеДействияФормыДействиеЗаполнить(Кнопка)	
	
	Если ЕстьСтрокиВДокументе() Тогда
		Если ФлажокТовары Тогда
			ДокументОбъект.Товары.Очистить();
		КонецЕсли;
		
		Если ФлажокУслуги Тогда
			ДокументОбъект.Услуги.Очистить();
		КонецЕсли;
		
		Если ФлажокТара Тогда
			ДокументОбъект.ВозвратнаяТара.Очистить();
		КонецЕсли;
		
	КонецЕсли;
	
	ОткрытьФормуОтбораЗаказов();
	
КонецПроцедуры

// Процедура обработчик события Нажатие кнопки "Записать и добавить".
//
Процедура ОсновныеДействияФормыДействиеДобавить(Кнопка)

	ОткрытьФормуОтбораЗаказов();
	
КонецПроцедуры

// Процедура обработчик события формы "Перед открытием".
//
Процедура ПередОткрытием(Отказ, СтандартнаяОбработка)

	Если НЕ ЗначениеЗаполнено(ДокументОбъект.Организация) Тогда
		Предупреждение("Не заполнено значение реквизита ""Организация""!",,ЭтаФорма.Заголовок);
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(ДокументОбъект.Контрагент) Тогда
		Предупреждение("Не заполнено значение реквизита ""Контрагент""!",,ЭтаФорма.Заголовок);
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(ДокументОбъект.ДоговорКонтрагента) Тогда
		Предупреждение("Не заполнено значение реквизита ""Договор контрагента""!",,ЭтаФорма.Заголовок);
		Отказ = Истина;
		Возврат;
	КонецЕсли;

КонецПроцедуры

// Процедура обработчик события формы "При открытии".
//
Процедура ПриОткрытии()
	
	мИспользоватьТару = глЗначениеПеременной("ИспользоватьВозвратнуюТару");
	
	УстановитьДоступностьЭлементовПанелиОтбораНоменклатуры();
	УстановитьДоступностьКнопок();
	ЭлементыФормы.НадписьВнимание.Видимость = ДокументОбъект.Модифицированность();
    ЭлементыФормы.НадписьВнимание.Заголовок = ?(ДокументОбъект.Проведен,"Внимание! Документ будет проведен.", "Внимание! Документ будет записан.");
	
КонецПроцедуры

// Процедура обработчик события "При изменении" реквизита ФлажокТовары.
//
Процедура ФлажокТоварыПриИзменении(Элемент)
	
	УстановитьДоступностьКнопок();
	
КонецПроцедуры

// Процедура обработчик события "При изменении" реквизита ФлажокУслуги.
//
Процедура ФлажокУслугиПриИзменении(Элемент)
	
	УстановитьДоступностьКнопок();
	
КонецПроцедуры

// Процедура обработчик события "При изменении" реквизита ФлажокТара.
//
Процедура ФлажокТараПриИзменении(Элемент)
	
	УстановитьДоступностьКнопок();
	
КонецПроцедуры

