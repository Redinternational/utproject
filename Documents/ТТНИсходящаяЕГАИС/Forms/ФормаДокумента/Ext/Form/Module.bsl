﻿
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Документы.ТТНИсходящаяЕГАИС.УстановитьУсловноеОформлениеСтатусаОбработки(ЭтаФорма, "СтатусОбработки", "Объект.СтатусОбработки");
	
	ПроверитьИнформациюОбОшибке();
	
	ДоступностьЭлементовФормы();
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПроверитьИнформациюОбОшибке();
	
	ДоступностьЭлементовФормы();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ВыгрузитьТТН(Команда)
	
	Если Модифицированность Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Документ не сохранен.'"));
		Возврат;
	КонецЕсли;
	
	ПроведенПередОтправкой = Ложь;
	
	Если НЕ Объект.Проведен Тогда
		Если НЕ Записать(Новый Структура("РежимЗаписи", РежимЗаписиДокумента.Проведение)) Тогда
			Возврат;
		КонецЕсли;
		
		ПроведенПередОтправкой = Истина;
	КонецЕсли;
	
	ВыгрузитьДокумент(ПредопределенноеЗначение("Перечисление.ВидыДокументовЕГАИС.ТТН"), ПроведенПередОтправкой);
	
КонецПроцедуры

&НаКлиенте
Процедура ПодтвердитьАктРасхождений(Команда)
	
	ВыгрузитьДокумент(ПредопределенноеЗначение("Перечисление.ВидыДокументовЕГАИС.ПодтверждениеАктаРасхожденийТТН"));
	
КонецПроцедуры

&НаКлиенте
Процедура ОтказатьсяОтАктаРасхождений(Команда)
	
	ВыгрузитьДокумент(ПредопределенноеЗначение("Перечисление.ВидыДокументовЕГАИС.ОтказОтАктаРасхожденийТТН"));
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьСправкиБ(Команда)
	
	Если Объект.Грузоотправитель.Пустая() Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Не выбран грузоотправитель'"));
		Возврат;
	КонецЕсли;
	
	ПодобратьСправкиБНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОтказатьсяОтТТН(Команда)
	
	ВыгрузитьДокумент(ПредопределенноеЗначение("Перечисление.ВидыДокументовЕГАИС.АктОтказаОтТТН"));
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ

&НаКлиенте
Процедура ТоварыКоличествоПриИзменении(Элемент)
	
	РассчитатьСуммуВСтроке();
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЦенаПриИзменении(Элемент)
	
	РассчитатьСуммуВСтроке();
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСуммаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Если ТекущаяСтрока = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ТекущаяСтрока.Количество <> 0 Тогда
		ТекущаяСтрока.Цена = ТекущаяСтрока.Сумма / ТекущаяСтрока.Количество;
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

&НаСервере
Процедура ДоступностьЭлементовФормы()
	
	РедактируемыеСтатусы = Новый Массив;
	РедактируемыеСтатусы.Добавить(Перечисления.СтатусыОбработкиТТНИсходящейЕГАИС.Новый);
	РедактируемыеСтатусы.Добавить(Перечисления.СтатусыОбработкиТТНИсходящейЕГАИС.ОшибкаПередачиВЕГАИС);
	
	ТолькоПросмотр = РедактируемыеСтатусы.Найти(Объект.СтатусОбработки) = Неопределено;
	
	СтатусыПодтвержденияАкта = Новый Массив;
	СтатусыПодтвержденияАкта.Добавить(Перечисления.СтатусыОбработкиТТНИсходящейЕГАИС.ПринятАктРасхождений);
	СтатусыПодтвержденияАкта.Добавить(Перечисления.СтатусыОбработкиТТНИсходящейЕГАИС.ОшибкаПередачиПодтвержденияАктаРасхождений);
	СтатусыПодтвержденияАкта.Добавить(Перечисления.СтатусыОбработкиТТНИсходящейЕГАИС.ОшибкаПередачиОтказаОтАктаРасхождений);
	
	Элементы.ФормаВыгрузитьТТН.Видимость = РедактируемыеСтатусы.Найти(Объект.СтатусОбработки) <> Неопределено;
	Элементы.ФормаОтказатьсяОтТТН.Видимость = Объект.СтатусОбработки = Перечисления.СтатусыОбработкиТТНИсходящейЕГАИС.ПереданВЕГАИС;
	Элементы.ФормаПодтвердитьАктРасхождений.Видимость = СтатусыПодтвержденияАкта.Найти(Объект.СтатусОбработки) <> Неопределено;
	Элементы.ФормаОтказатьсяОтАктаРасхождений.Видимость = СтатусыПодтвержденияАкта.Найти(Объект.СтатусОбработки) <> Неопределено;
	
	Элементы.ТоварыПодобратьСправкиБ.Доступность = НЕ ТолькоПросмотр;
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьИнформациюОбОшибке()

	ИнформацияОбОшибке = РегистрыСведений.ПротоколОбменаЕГАИС.ТекстПоследнейОшибки(Объект.Ссылка);
	Элементы.ГруппаИнформацияОбОшибке.Видимость = НЕ ПустаяСтрока(ИнформацияОбОшибке);

КонецПроцедуры

&НаКлиенте
Процедура ВыгрузитьДокумент(ВидДокумента, ПроведенПередОтправкой = Ложь)
	
	Если Модифицированность ИЛИ НЕ Объект.Проведен Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Документ не проведен.'"));
		Возврат;
	КонецЕсли;
	
	ПараметрыЗапроса = ИнтеграцияЕГАИСКлиентСервер.ПараметрыИсходящегоЗапроса(ВидДокумента);
	ПараметрыЗапроса.ДокументСсылка = Объект.Ссылка;
	
	Результат = ИнтеграцияЕГАИСКлиент.СформироватьИсходящийЗапрос(ВидДокумента, ПараметрыЗапроса);
	
	Если Результат.Результат Тогда
		Прочитать();
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Документ успешно выгружен в УТМ.'"));
	ИначеЕсли ПроведенПередОтправкой Тогда
		Записать(Новый Структура("РежимЗаписи", РежимЗаписиДокумента.ОтменаПроведения));
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПодобратьСправкиБНаСервере()
	
	ДатаОстатков = ?(ЗначениеЗаполнено(Объект.Ссылка), Новый Граница(Объект.Дата, ВидГраницы.Исключая), Неопределено);
	
	ИнтеграцияЕГАИС.ПодобратьСправкиБДляСписания(Объект.Товары, Объект.Грузоотправитель, ДатаОстатков);
	
КонецПроцедуры

&НаКлиенте
Процедура РассчитатьСуммуВСтроке()
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Если ТекущаяСтрока = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущаяСтрока.Сумма = ТекущаяСтрока.Цена * ТекущаяСтрока.Количество;
	
КонецПроцедуры
