﻿////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обаботчик события "ПередОткрытием" Формы
//
Процедура ПередОткрытием(Отказ, СтандартнаяОбработка)
	
	ЭлектронныеДокументы.ПередОткрытиемВходящегоДокументаВФорме(Отказ, СтандартнаяОбработка, ЭтотОбъект, ЭтаФорма);	
	ЭлектронныеДокументы.ПередОткрытиемДокументаПоКоторомуМожетБытьсозданОтвет(СтандартнаяОбработка, ЭтотОбъект, ЭтаФорма);
	
	ТолькоПросмотр = СуществуетИсходящийДокумент();
	
КонецПроцедуры

// Процедура - обаботчик события "ПослеЗаписи" Формы
//
Процедура ПослеЗаписи() Экспорт
	
	ЭлектронныеДокументы.ПослеЗаписиФормыДокумента(ЭтотОбъект, ЭтаФорма);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ

// Процедура - обаботчик события, при нажатии на кнопку "Структура подчиненности документа" Командной панели "ДействияФормы"
//
Процедура ДействияФормыСтруктураПодчиненностиДокумента(Кнопка)
	
	РаботаСДиалогами.ПоказатьСтруктуруПодчиненностиДокумента(Ссылка);
	
КонецПроцедуры

// Процедура - обаботчик события, при нажатии на кнопку "Реджект/Акцепт накладной" Командной панели "ОсновныеДействияФормы"
//
Процедура ОсновныеДействияФормыРеджектАкцептНакладной(Кнопка)
	
	СсылкаНаОтветныйДокумент = Неопределено;
	ОтветныйДокументНайден = СуществуетИсходящийДокумент(СсылкаНаОтветныйДокумент);
	
	Если НЕ ОтветныйДокументНайден Тогда
	
		НачатьТранзакцию();
		
		Обработан = Истина;
		Попытка
			Записать();
		Исключение
			ОбщегоНазначения.СообщитьОбОшибке(ОписаниеОшибки());
			ОтменитьТранзакцию();
			Предупреждение("Операция не выполнена");
			Возврат;
		КонецПопытки;
		
		Документ = Документы.ИсходящийРеджектАкцептСчетаФактуры.СоздатьДокумент();
		Документ.Заполнить(Ссылка);
		
		ЗафиксироватьТранзакцию();		
		
	Иначе
		Документ = СсылкаНаОтветныйДокумент;
		
		Если Не Обработан Тогда
			Обработан = Истина;
			Попытка
				Записать();
			Исключение
				ОбщегоНазначения.СообщитьОбОшибке(ОписаниеОшибки());
			КонецПопытки;
		КонецЕсли;
		
	КонецЕсли;
	
	Документ.ПолучитьФорму(, ВладелецФормы).Открыть();
	ПослеЗаписи();
	Закрыть();	
		
КонецПроцедуры

// обработчик показа состава файла обмена
Процедура ДействияФормыПоказатьДанныеОбмена(Кнопка)
	
	ЭлектронныеДокументы.ПоказатьДанныеОбмена(ЭтотОбъект);	
	
КонецПроцедуры

// форма параметров электронного обмена документами
Процедура ДействияФормыПараметрыЭлектронногоОбменаДокументами(Кнопка)
	
	ЭлектронныеДокументы.ПоказатьПараметрыВходящегоДокумента(ЭтотОбъект);	
	
КонецПроцедуры

// показ примечания
Процедура ПримечаниеОткрытие(Элемент, СтандартнаяОбработка)
	
	ЭлектронныеДокументы.ПоказатьДлинныеСтроковыеДанные(ЭтаФорма, Элемент, СтандартнаяОбработка);
	
КонецПроцедуры

// показ файлов приложенных к документу
Процедура ДействияФормыФайлы(Кнопка)
	
	ЭлектронныеДокументы.ПоказатьФайлыКСсылкеНаОбъект(ЭтаФорма, Ссылка);	
	
КонецПроцедуры

Процедура ПриОткрытии()
	Если НЕ ЭтоНовый() Тогда
		НастройкаПравДоступа.ОпределитьДоступностьВозможностьИзмененияДокументаПоДатеЗапрета(ДокументОбъект, ЭтаФорма);
	КонецЕсли;
КонецПроцедуры
