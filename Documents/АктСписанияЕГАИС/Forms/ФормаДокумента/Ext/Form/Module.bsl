﻿
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Документы.АктСписанияЕГАИС.УстановитьУсловноеОформлениеСтатусаОбработки(ЭтаФорма, "СтатусОбработки", "Объект.СтатусОбработки");
	
	Если НЕ ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПроверитьИнформациюОбОшибке();
		
		ДоступностьЭлементовФормы();
	КонецЕсли;
	
	ИспользуемыйФорматОбмена = ФорматОбменаОрганизацииЕГАИС(Объект.ОрганизацияЕГАИС);
	
	ОбновитьПричиныСписанияНаСервере();
	
	КлючСвязи = КлючСвязи + 1;

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Объект.Ссылка);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	АктСписанияЕГАИСТовары.КлючСвязи КАК КлючСвязи
	|ИЗ
	|	Документ.АктСписанияЕГАИС.Товары КАК АктСписанияЕГАИСТовары
	|ГДЕ
	|	АктСписанияЕГАИСТовары.Ссылка = &Ссылка";
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		КлючСвязи = Макс(КлючСвязи, Число(Выборка.КлючСвязи));
	КонецЦикла;

	ПроверитьИнформациюОбОшибке();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	УстановитьОтборАкцизныхМарок();
	
	ДоступностьЭлементовФормы();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ

&НаКлиенте
Процедура ТоварыСправкаБАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	
	ПараметрыПолученияДанных.Отбор.Очистить();
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСправкаБПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	Если НЕ ТекущиеДанные = Неопределено Тогда
		Если ЗначениеЗаполнено(ТекущиеДанные.СправкаБ) Тогда
			ТекущиеДанные.АлкогольнаяПродукция = ИнтеграцияЕГАИСВызовСервера.ЗначениеРеквизитаОбъекта(ТекущиеДанные.СправкаБ, "АлкогольнаяПродукция");
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияЕГАИСПриИзменении(Элемент)
	
	ИспользуемыйФорматОбмена = ФорматОбменаОрганизацииЕГАИС(Объект.ОрганизацияЕГАИС);
	
	ОбновитьПричиныСписанияНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриАктивизацииСтроки(Элемент)
	
	УстановитьОтборАкцизныхМарок();
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	Если НоваяСтрока Тогда
		Элемент.ТекущиеДанные.КлючСвязи = КлючСвязи;
		КлючСвязи = КлючСвязи + 1;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПередУдалением(Элемент, Отказ)
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено ИЛИ ТекущиеДанные.КлючСвязи = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого АМ Из Объект.АкцизныеМарки Цикл
		Если АМ.КлючСвязи = ТекущиеДанные.КлючСвязи Тогда
			Объект.АкцизныеМарки.Удалить(АМ);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ВидДокументаПриИзменении(Элемент)
	
	ОбновитьПричиныСписанияНаКлиенте();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ВыгрузитьАкт(Команда)
	
	Если Модифицированность Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Документ не сохранен.'"));
		Возврат;
	КонецЕсли;
	
	ПроведенПередОтправкой = Ложь;
	
	Если НЕ Объект.Проведен Тогда
		Если НЕ Записать(Новый Структура("РежимЗаписи", РежимЗаписиДокумента.Проведение)) Тогда
			Возврат;
		КонецЕсли;
		
		ПроведенПередОтправкой = Истина;
	КонецЕсли;
	
	ВидДокумента = Объект.ВидДокумента;
	
	ПараметрыЗапроса = ИнтеграцияЕГАИСКлиентСервер.ПараметрыИсходящегоЗапроса(ВидДокумента);
	ПараметрыЗапроса.ДокументСсылка = Объект.Ссылка;
	
	Результат = ИнтеграцияЕГАИСКлиент.СформироватьИсходящийЗапрос(ВидДокумента, ПараметрыЗапроса);
	
	Если Результат.Результат Тогда
		Прочитать();
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Документ успешно выгружен.'"));
	ИначеЕсли ПроведенПередОтправкой Тогда
		Записать(Новый Структура("РежимЗаписи", РежимЗаписиДокумента.ОтменаПроведения));
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗапроситьОтменуПроведения(Команда)
	
	Если Модифицированность Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Документ не сохранен.'"));
		Возврат;
	КонецЕсли;
	
	ВидДокумента = ПредопределенноеЗначение("Перечисление.ВидыДокументовЕГАИС.ЗапросНаОтменуПроведенияАктаСписания");
	
	ПараметрыЗапроса = ИнтеграцияЕГАИСКлиентСервер.ПараметрыИсходящегоЗапроса(ВидДокумента);
	ПараметрыЗапроса.ДокументСсылка = Объект.Ссылка;
	
	Результат = ИнтеграцияЕГАИСКлиент.СформироватьИсходящийЗапрос(ВидДокумента, ПараметрыЗапроса);
	
	Если Результат.Результат Тогда
		Прочитать();
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Запрос на отмену проведения успешно выгружен.'"));
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УказатьАкцизнуюМарку(Команда)
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ МаркируемаяПродукция(ТекущиеДанные.АлкогольнаяПродукция) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Ввод акцизной марки возможен только для маркируемой алкогольной продукции.'"));
		Возврат;
	КонецЕсли;
	
	КодАкцизнойМарки = "";
	ВвестиСтроку(КодАкцизнойМарки, НСтр("ru = 'Введите акцизную марку'"), 68);
	
	Если НЕ ЗначениеЗаполнено(КодАкцизнойМарки) Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	СтрокаТЧ = Объект.АкцизныеМарки.Добавить();
	СтрокаТЧ.КодАкцизнойМарки = КодАкцизнойМарки;
	СтрокаТЧ.КлючСвязи = ТекущиеДанные.КлючСвязи;
	
	УстановитьОтборАкцизныхМарок();
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьАкцизнуюМарку(Команда)
	
	ТекущиеДанные = Элементы.АкцизныеМарки.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Объект.АкцизныеМарки.Удалить(ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьАкцизнуюМарку(Команда)
	
	СтрокаТабличнойЧасти = ЭтаФорма.Элементы.Товары.ТекущиеДанные;
	Если СтрокаТабличнойЧасти = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ФормаСканированияМарки = ПолучитьФорму("ОбщаяФорма.ФормаСканированияАкцизнойМарки", , ЭтаФорма);
	ФормаСканированияМарки.КлючСтроки = СтрокаТабличнойЧасти.КлючСвязи;
	ШтрихкодМарки = ФормаСканированияМарки.ОткрытьМодально();
	
	Если ЗначениеЗаполнено(ШтрихкодМарки) Тогда
		СтрокаАкцизныеМарки = Объект.АкцизныеМарки.Добавить();
		СтрокаАкцизныеМарки.КлючСвязи = СтрокаТабличнойЧасти.КлючСвязи;
		СтрокаАкцизныеМарки.КодАкцизнойМарки = ШтрихкодМарки;
		УстановитьОтборАкцизныхМарок();
		ЭтаФорма.ОбновитьОтображениеДанных();
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

&НаКлиенте
Процедура УстановитьОтборАкцизныхМарок()
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Элементы.АкцизныеМарки.ОтборСтрок = Неопределено;
	Иначе
		Элементы.АкцизныеМарки.ОтборСтрок = Новый ФиксированнаяСтруктура("КлючСвязи", ТекущиеДанные.КлючСвязи);
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ФорматОбменаОрганизацииЕГАИС(ОрганизацияЕГАИС)
	
	Результат = Перечисления.ФорматыОбменаЕГАИС.V1;
	Если НЕ ЗначениеЗаполнено(ОрганизацияЕГАИС) Тогда
		Возврат Результат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", ОрганизацияЕГАИС);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ФорматыОбменаЕГАИС.ФорматОбмена КАК ФорматОбмена
	|ИЗ
	|	Справочник.КлассификаторОрганизацийЕГАИС КАК КлассификаторОрганизацийЕГАИС
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ФорматыОбменаЕГАИС КАК ФорматыОбменаЕГАИС
	|		ПО КлассификаторОрганизацийЕГАИС.Код = ФорматыОбменаЕГАИС.ИдентификаторФСРАР
	|ГДЕ
	|	КлассификаторОрганизацийЕГАИС.Ссылка = &Ссылка";
	
	РезультатЗапроса = Запрос.Выполнить();
	Если НЕ РезультатЗапроса.Пустой() Тогда
		Возврат РезультатЗапроса.Выгрузить()[0].ФорматОбмена;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

&НаКлиенте
Процедура ОбновитьПричиныСписанияНаКлиенте()
	
	Если ИспользуемыйФорматОбмена = ПредопределенноеЗначение("Перечисление.ФорматыОбменаЕГАИС.V2")
		ИЛИ ИспользуемыйФорматОбмена = ПредопределенноеЗначение("Перечисление.ФорматыОбменаЕГАИС.V3") Тогда
		РасширенныйСписок = Истина;
	Иначе
		РасширенныйСписок = Ложь;
	КонецЕсли;
	
	СписокПричин = Элементы.ПричинаСписания.СписокВыбора;
	
	ПричинаИныеЦели = ПредопределенноеЗначение("Перечисление.ПричиныСписанийЕГАИС.ИныеЦели");
	ПричинаРеализация = ПредопределенноеЗначение("Перечисление.ПричиныСписанийЕГАИС.Реализация");
	
	ЭлементСпискаИныеЦели = СписокПричин.НайтиПоЗначению(ПричинаИныеЦели);
	ЭлементСпискаРеализация = СписокПричин.НайтиПоЗначению(ПричинаРеализация);
	
	Если РасширенныйСписок Тогда
		Если ЭлементСпискаИныеЦели = Неопределено Тогда
			СписокПричин.Добавить(ПричинаИныеЦели);
		КонецЕсли;
		Если ЭлементСпискаРеализация = Неопределено Тогда
			СписокПричин.Добавить(ПричинаРеализация);
		КонецЕсли;
	Иначе
		Если ЭлементСпискаИныеЦели <> Неопределено Тогда
			СписокПричин.Удалить(ЭлементСпискаИныеЦели);
		КонецЕсли;
		Если ЭлементСпискаРеализация <> Неопределено Тогда
			СписокПричин.Удалить(ЭлементСпискаРеализация);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьПричиныСписанияНаСервере()
	
	РасширенныйСписок = ИспользуемыйФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2;
	
	СписокПричин = Элементы.ПричинаСписания.СписокВыбора;
	
	ЭлементСпискаИныеЦели = СписокПричин.НайтиПоЗначению(Перечисления.ПричиныСписанийЕГАИС.ИныеЦели);
	ЭлементСпискаРеализация = СписокПричин.НайтиПоЗначению(Перечисления.ПричиныСписанийЕГАИС.Реализация);
	
	Если РасширенныйСписок Тогда
		Если ЭлементСпискаИныеЦели = Неопределено Тогда
			СписокПричин.Добавить(Перечисления.ПричиныСписанийЕГАИС.ИныеЦели);
		КонецЕсли;
		Если ЭлементСпискаРеализация = Неопределено Тогда
			СписокПричин.Добавить(Перечисления.ПричиныСписанийЕГАИС.Реализация);
		КонецЕсли;
	Иначе
		Если ЭлементСпискаИныеЦели <> Неопределено Тогда
			СписокПричин.Удалить(ЭлементСпискаИныеЦели);
		КонецЕсли;
		Если ЭлементСпискаРеализация <> Неопределено Тогда
			СписокПричин.Удалить(ЭлементСпискаРеализация);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьИнформациюОбОшибке()

	ИнформацияОбОшибке = РегистрыСведений.ПротоколОбменаЕГАИС.ТекстПоследнейОшибки(Объект.Ссылка);
	Элементы.ИнформацияОбОшибке.Видимость = НЕ ПустаяСтрока(ИнформацияОбОшибке);

КонецПроцедуры

&НаСервере
Процедура ДоступностьЭлементовФормы()
	
	ТолькоПросмотр =
		Объект.СтатусОбработки <> Перечисления.СтатусыОбработкиАктаСписанияЕГАИС.Новый
		И Объект.СтатусОбработки <> Перечисления.СтатусыОбработкиАктаСписанияЕГАИС.ОшибкаПередачиВЕГАИС;
		
	Элементы.ФормаВыгрузитьАкт.Видимость = НЕ ТолькоПросмотр;
	Элементы.ФормаЗапроситьОтменуПроведения.Видимость =
		Объект.СтатусОбработки = Перечисления.СтатусыОбработкиАктаСписанияЕГАИС.ПереданВЕГАИС
		ИЛИ Объект.СтатусОбработки = Перечисления.СтатусыОбработкиАктаСписанияЕГАИС.ОшибкаПередачиЗапросаНаОтменуПроведения;
		
КонецПроцедуры

&НаСервереБезКонтекста
Функция МаркируемаяПродукция(АлкогольнаяПродукция)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", АлкогольнаяПродукция);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ЕСТЬNULL(КлассификаторАлкогольнойПродукцииЕГАИС.ВидПродукции.Маркируемый, ЛОЖЬ) КАК Маркируемый
	|ИЗ
	|	Справочник.КлассификаторАлкогольнойПродукцииЕГАИС КАК КлассификаторАлкогольнойПродукцииЕГАИС
	|ГДЕ
	|	КлассификаторАлкогольнойПродукцииЕГАИС.Ссылка = &Ссылка";
	
	Выборка = Запрос.Выполнить().Выбрать();
	Выборка.Следующий();
	
	Возврат ?(ТипЗнч(Выборка.Маркируемый) = Тип("Булево"), Выборка.Маркируемый, Ложь);
	
КонецФункции

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	ИнтеграцияЕГАИСПереопределяемый.ПроверитьЗаполнениеАкцизныхМарок(Объект, Отказ);
	Если Объект.ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктСписания Тогда
		ИнтеграцияЕГАИСПереопределяемый.ПроверитьЗаполнениеСправокБ(Объект, Отказ);
	КонецЕсли;
КонецПроцедуры
