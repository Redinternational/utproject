﻿////////////////////////////////////////////////////////////////////////////////
// ПЕРЕМЕННЫЕ МОДУЛЯ

Перем мИспользоватьОбменЭД; // Переменная содержит значение функциональной опции "ИспользоватьОбменЭД"

//  Процедура печатает выбранный документ 
// Печатается та форма, которая была отпечатана при нажатии в документе кнопки
// печати по умолчанию
//
Процедура ДействияФормыДействиеПечать(Кнопка)

	Если ЭтаФорма.ЭлементыФормы.Список.ТекущаяСтрока = Неопределено тогда
		Возврат
	КонецЕсли;

	УниверсальныеМеханизмы.НапечататьДокументИзФормыСписка(ЭтаФорма.ЭлементыФормы.Список.ТекущаяСтрока.ПолучитьОбъект());

КонецПроцедуры // ДействиеПечать()

// Процедура вызывается при выборе пункта подменю "Движения документа по регистрам" меню "Перейти".
// командной панели формы. Процедура отрабатывает печать движений документа по регистрам.
//
Процедура ДействияФормыДвиженияДокументаПоРегистрам(Кнопка)

	Если ЭтаФорма.ЭлементыФормы.Список.ТекущаяСтрока = Неопределено тогда
		Возврат
	КонецЕсли;

	РаботаСДиалогами.НапечататьДвиженияДокумента(ЭлементыФормы.Список.ТекущиеДанные.Ссылка);

КонецПроцедуры // ДействияФормыДвиженияДокументаПоРегистрам()

// Процедура вызывается при выборе пункта подменю "Структура подчиненности" меню "Перейти".
Процедура ДействияФормыСтруктураПодчиненностиДокумента(Кнопка)
	
	Если ЭлементыФормы.Список.ТекущиеДанные = Неопределено тогда
		Возврат
	КонецЕсли;
	
	РаботаСДиалогами.ПоказатьСтруктуруПодчиненностиДокумента(ЭтаФорма.ЭлементыФормы.Список.ТекущаяСтрока);
	
КонецПроцедуры

// Процедура вызывается при нажатии на кнопку "файлы"
Процедура ДействияФормыФайлы(Кнопка)
	
	Если ЭлементыФормы.Список.ТекущаяСтрока = Неопределено Тогда
		Возврат;
	КонецЕсли; 
	
	Ссылка = ЭлементыФормы.Список.ТекущаяСтрока.Ссылка;
	ФормаФайлов = Справочники.ХранилищеДополнительнойИнформации.ПолучитьФорму("ФормаСпискаФайловИИзображений", ЭтаФорма);
	
	ФормаФайлов.Изображения.Отбор.Объект.Использование                               = Истина;
	ФормаФайлов.Изображения.Отбор.Объект.Значение                                    = Ссылка;
	ФормаФайлов.ЭлементыФормы.Изображения.НастройкаОтбора.Объект.Доступность         = Ложь;
	ФормаФайлов.ЭлементыФормы.Изображения.Колонки.Объект.Видимость                   = Ложь;

	ФормаФайлов.ДополнительныеФайлы.Отбор.Объект.Использование                       = Истина;
	ФормаФайлов.ДополнительныеФайлы.Отбор.Объект.Значение                            = Ссылка;
	ФормаФайлов.ЭлементыФормы.ДополнительныеФайлы.НастройкаОтбора.Объект.Доступность = Ложь;
	ФормаФайлов.ЭлементыФормы.ДополнительныеФайлы.Колонки.Объект.Видимость           = Ложь;

	ОбязательныеОтборы = Новый Структура;
	ОбязательныеОтборы.Вставить("Объект",Ссылка);

	ФормаФайлов.ОбязательныеОтборы = ОбязательныеОтборы;
	
	ФормаФайлов.Открыть();
	
КонецПроцедуры

Процедура ДействияФормыДействиеАнализ(Кнопка)
	Если ЭлементыФормы.Список.ТекущиеДанные = Неопределено тогда
		Возврат
	КонецЕсли;
	УправлениеЗаказами.СформироватьОтчетАнализЗаказа(ЭлементыФормы.Список.ТекущиеДанные.Ссылка,ложь, истина);

КонецПроцедуры

// Процедура - обработчик действия элемента "СогласованиеЗаказа" панели "ДействияФормы".
//
Процедура ДействияФормыСогласованиеЗаказа(Кнопка)

	ТекущиеДанные = ЭлементыФормы.Список.ТекущиеДанные;

	Если ТекущиеДанные <> Неопределено Тогда
		РаботаСБизнесПроцессами.ОткрытьСписокСогласованиеЗаказаПокупателяПоДокументу(ТекущиеДанные.Ссылка);
	КонецЕсли;

КонецПроцедуры // ДействияФормыСогласованиеЗаказа()

//////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ для работы с электронными документами

Процедура ДействияФормыПодписатьИОтправить(Кнопка)
	
	Если ЭлементыФормы.Список.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЭлектронныеДокументыКлиент.СформироватьПодписатьОтправитьЭД(ЭлементыФормы.Список.ТекущиеДанные.Ссылка);

КонецПроцедуры

Процедура ДействияФормыСформироватьНовый(Кнопка)
	
	Если ЭлементыФормы.Список.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЭлектронныеДокументыКлиент.СформироватьНовыйЭД(ЭлементыФормы.Список.ТекущиеДанные.Ссылка);
	
КонецПроцедуры

Процедура ДействияФормыОткрытьАктуальныйЭД(Кнопка)
	
	Если ЭлементыФормы.Список.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;

	ЭлектронныеДокументыКлиент.ОткрытьАктуальныйЭД(ЭлементыФормы.Список.ТекущиеДанные.Ссылка, ЭтаФорма);
	
КонецПроцедуры

Процедура ДействияФормыПерезаполнитьДаннымиИзЭД(Кнопка)
	
	Если ЭлементыФормы.Список.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЭлектронныеДокументыКлиент.ПерезаполнитьДокумент(ЭлементыФормы.Список.ТекущиеДанные.Ссылка, ЭтаФорма);
	
КонецПроцедуры

Процедура ДействияФормыСписокЭлектронныхДокументов(Кнопка)
	
	Если ЭлементыФормы.Список.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Ссылка = ЭлементыФормы.Список.ТекущиеДанные.Ссылка;
	
	ПараметрыОткрытия = Новый Структура;
	ПараметрыОткрытия.Вставить("Уникальность", 	Ссылка.УникальныйИдентификатор());
	ПараметрыОткрытия.Вставить("Источник", 		ЭтаФорма);
	
	ЭлектронныеДокументыКлиент.ОткрытьСписокЭД(Ссылка, ПараметрыОткрытия);
	
КонецПроцедуры

Процедура Подключаемый_ОбработчикОжиданияЭДО()
	
	ЭлектронныеДокументыКлиентПереопределяемый.Подключаемый_ОбработчикОжиданияЭДО(ЭтаФорма);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

Процедура ПередОткрытием(Отказ, СтандартнаяОбработка)
	
	// Начало ЭлектронныеДокументы
	РаботаСДиалогами.УдалитьКнопкуЭД(ЭлементыФормы.ДействияФормы.Кнопки, мИспользоватьОбменЭД);
	// Конец ЭлектронныеДокументы
	
КонецПроцедуры

Процедура ПриОткрытии()
	
	// Начало ЭлектронныеДокументы
	ЭлектронныеДокументыКлиент.ПриОткрытии(ЭтаФорма);
	ЭлектронныеДокументыПереопределяемый.ЭДО_ФормаСпискаДокумента_ПриОткрытии(ЭтаФорма, мИспользоватьОбменЭД);
	// Конец ЭлектронныеДокументы
	
КонецПроцедуры

Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// Начало ЭлектронныеДокументы
	
	Если ИмяСобытия = "ОбновитьСостояниеЭД" Тогда
		ДокументСписок.Обновить();	
	Иначе // ИмяСобытия = "ОбновитьОСобытияхЭДО"
		ЭлектронныеДокументыКлиентПереопределяемый.ОбработкаОповещения_ФормаСписка(ИмяСобытия, Параметр, Источник, ЭтаФорма);
	КонецЕсли;	
	
	// Конец ЭлектронныеДокументы
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ списка документов

Процедура СписокПриПолученииДанных(Элемент, ОформленияСтрок)
	
	// Начало ЭлектронныеДокументы
	ЭлектронныеДокументыПереопределяемый.ЭДО_ФормаСпискаДокумента_ПриПолученииДанных(Элемент, ОформленияСтрок, мИспользоватьОбменЭД);
	// Конец ЭлектронныеДокументы

КонецПроцедуры

мИспользоватьОбменЭД = ЭлектронныеДокументыСлужебныйВызовСервера.ПолучитьЗначениеФункциональнойОпции("ИспользоватьОбменЭД");