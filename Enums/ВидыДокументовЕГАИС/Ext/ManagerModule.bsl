﻿
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЙ ПРОГРАММНЫЙ ИНТЕРФЕЙС

// Возвращает адрес исходящего запроса для выгрузки документа в УТМ.
//
Функция АдресЗапроса(ВидДокумента, ФорматОбмена, ТекстОшибки) Экспорт
	
	Если ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктПодтвержденияТТН
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктРасхожденийТТН
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктОтказаОтТТН Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/WayBillAct_v3";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/WayBillAct_v2";
		Иначе
			Возврат "/opt/in/WayBillAct";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктПостановкиНаБаланс Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/ActChargeOn_v2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/ActChargeOn_v2";
		Иначе
			Возврат "/opt/in/ActChargeOn";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктПостановкиНаБалансВТорговомЗале Тогда
		Возврат "/opt/in/ActChargeOnShop_v2";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктСписания Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/ActWriteOff_v2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/ActWriteOff_v2";
		Иначе
			Возврат "/opt/in/ActWriteOff";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктСписанияИзТорговогоЗала Тогда
		Возврат "/opt/in/ActWriteOffShop_v2";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ВозвратИзТорговогоЗала Тогда
		Возврат "/opt/in/TransferFromShop";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросАлкогольнойПродукции Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/QueryAP_v2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/QueryAP_v2";
		Иначе
			Возврат "/opt/in/QueryAP";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросОрганизаций Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/QueryClients_v2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/QueryClients_v2";
		Иначе
			Возврат "/opt/in/QueryPartner";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросОстатков Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/QueryRests_v2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/QueryRests_v2";
		Иначе
			Возврат "/opt/in/QueryRests";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросОстатковВТорговомЗале Тогда
		Возврат "/opt/in/QueryRestsShop_v2";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросСправкиА Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/QueryFormF1";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/QueryFormF1";
		Иначе
			Возврат "/opt/in/QueryFormA";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросСправкиБ Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/QueryFormF2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/QueryFormF2";
		Иначе
			Возврат "/opt/in/QueryFormB";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросТТН Тогда
		Возврат "/opt/in/QueryResendDoc";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросНаОтменуПроведенияТТН Тогда
		Возврат "/opt/in/RequestRepealWB";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросНаОтменуПроведенияАктаПостановкиНаБаланс Тогда
		Возврат "/opt/in/RequestRepealACO";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросНаОтменуПроведенияАктаСписания Тогда
		Возврат "/opt/in/RequestRepealAWO";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ИнформацияОФорматеОбмена Тогда
		Возврат "/opt/in/InfoVersionTTN";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ПередачаВТорговыйЗал Тогда
		Возврат "/opt/in/TransferToShop";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ПодтверждениеАктаРасхожденийТТН
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ОтказОтАктаРасхожденийТТН Тогда
		Возврат "/opt/in/WayBillTicket";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ТТН Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "/opt/in/WayBill_v3";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "/opt/in/WayBill_v2";
		Иначе
			Возврат "/opt/in/WayBill";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЧекККМ Тогда
		Возврат "/xml";
		
	Иначе
		ТекстОшибки = НСтр("ru='В функцию %1 передан некорректный параметр %2'");
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстОшибки, "ВидыДокументовЕГАИС.АдресЗапроса", ВидДокумента);
		Возврат Неопределено;
	КонецЕсли;
	
КонецФункции

Функция ПространствоИмен(ВидДокумента, ФорматОбмена, ТекстОшибки) Экспорт
	
	Если ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктОтказаОтТТН
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктПодтвержденияТТН
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктРасхожденийТТН Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "http://fsrar.ru/WEGAIS/ActTTNSingle_v3";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "http://fsrar.ru/WEGAIS/ActTTNSingle_v2";
		Иначе
			Возврат "http://fsrar.ru/WEGAIS/ActTTNSingle";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктПостановкиНаБаланс Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "http://fsrar.ru/WEGAIS/ActChargeOn_v2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "http://fsrar.ru/WEGAIS/ActChargeOn_v2";
		Иначе
			Возврат "http://fsrar.ru/WEGAIS/ActChargeOn";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктПостановкиНаБалансВТорговомЗале Тогда
		Возврат "http://fsrar.ru/WEGAIS/ActChargeOnShop_v2";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктСписания Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "http://fsrar.ru/WEGAIS/ActWriteOff_v2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "http://fsrar.ru/WEGAIS/ActWriteOff_v2";
		Иначе
			Возврат "http://fsrar.ru/WEGAIS/ActWriteOff";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.АктСписанияИзТорговогоЗала Тогда
		Возврат "http://fsrar.ru/WEGAIS/ActWriteOffShop_v2";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ВозвратИзТорговогоЗала Тогда
		Возврат "http://fsrar.ru/WEGAIS/TransferFromShop";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросАлкогольнойПродукции
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросОрганизаций
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросОстатков
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросОстатковВТорговомЗале
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросТТН Тогда
		Возврат "http://fsrar.ru/WEGAIS/QueryParameters";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросСправкиА
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросСправкиБ Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "http://fsrar.ru/WEGAIS/QueryFormF1F2";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "http://fsrar.ru/WEGAIS/QueryFormF1F2";
		Иначе
			Возврат "http://fsrar.ru/WEGAIS/QueryFormAB";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросНаОтменуПроведенияТТН Тогда
		Возврат "http://fsrar.ru/WEGAIS/RequestRepealWB";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросНаОтменуПроведенияАктаПостановкиНаБаланс Тогда
		Возврат "http://fsrar.ru/WEGAIS/RequestRepealACO";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЗапросНаОтменуПроведенияАктаСписания Тогда
		Возврат "http://fsrar.ru/WEGAIS/RequestRepealAWO";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ИнформацияОФорматеОбмена Тогда
		Возврат "http://fsrar.ru/WEGAIS/InfoVersionTTN";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ПередачаВТорговыйЗал Тогда
		Возврат "http://fsrar.ru/WEGAIS/TransferToShop";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ОтказОтАктаРасхожденийТТН
		ИЛИ ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ПодтверждениеАктаРасхожденийТТН Тогда
		Возврат "http://fsrar.ru/WEGAIS/ConfirmTicket";
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ТТН Тогда
		
		Если ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V3 Тогда
			Возврат "http://fsrar.ru/WEGAIS/TTNSingle_v3";
		ИначеЕсли ФорматОбмена = Перечисления.ФорматыОбменаЕГАИС.V2 Тогда
			Возврат "http://fsrar.ru/WEGAIS/TTNSingle_v2";
		Иначе
			Возврат "http://fsrar.ru/WEGAIS/TTNSingle";
		КонецЕсли;
		
	ИначеЕсли ВидДокумента = Перечисления.ВидыДокументовЕГАИС.ЧекККМ Тогда
		Возврат "egaischeque.joint.2";
		
	Иначе
		ТекстОшибки = НСтр("ru='В функцию %1 передан некорректный параметр %2'");
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстОшибки, "ВидыДокументовЕГАИС.ПространствоИмен", ВидДокумента);
		Возврат Неопределено;
	КонецЕсли;
	
КонецФункции

// Возвращает префикс переданного пространства имен.
//
Функция ПрефиксПространстваИмен(ПространствоИмен) Экспорт
	
	Если ВРег(ПространствоИмен) = ВРег("http://www.w3.org/2001/XMLSchema-instance") Тогда
		Возврат "xsi";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://www.w3.org/2001/XMLSchema") Тогда
		Возврат "xs";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActInventoryABInfo")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActInventoryF1F2Info") Тогда
		Возврат "iab";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ProductRef")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ProductRef_v2") Тогда
		Возврат "pref";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ClientRef")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ClientRef_v2") Тогда
		Возврат "oref";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01") Тогда
		Возврат "ns";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActTTNSingle")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActTTNSingle_v2")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActTTNSingle_v3") Тогда
		
		Возврат "wa";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActChargeOn")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActChargeOn_v2")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActChargeOnShop_v2") Тогда
		Возврат "ainp";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActWriteOff")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActWriteOff_v2")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ActWriteOffShop_v2") Тогда
		Возврат "awr";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/QueryParameters")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/RequestRepealWB")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/RequestRepealACO")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/RequestRepealAWO")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/InfoVersionTTN") Тогда
		Возврат "qp";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/QueryFormAB")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/QueryFormF1F2") Тогда
		Возврат "qf";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/ConfirmTicket") Тогда
		Возврат "wt";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/TTNSingle")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/TTNSingle_v2")
		ИЛИ ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/TTNSingle_v3") Тогда
		
		Возврат "wb";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/TransferToShop") Тогда
		Возврат "tts";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/TransferFromShop") Тогда
		Возврат "tfs";
		
	ИначеЕсли ВРег(ПространствоИмен) = ВРег("http://fsrar.ru/WEGAIS/CommonEnum") Тогда
		Возврат "non_qualified_element";
		
	Иначе
		Возврат "";
	КонецЕсли;
	
КонецФункции

#КонецЕсли