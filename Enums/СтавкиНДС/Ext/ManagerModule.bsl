﻿
// Определяет ставки НДС, операции, отражение по которым, подлежат регистрации в книге продаж.
//
// Параметры:
//   Параметры - Структура - параметры для определения ставок
//     * ВерсияПостановленияНДС1137 - Число - см. УчетНДС.ВерсияПостановленияНДС1137.
//
// Возвращаемое значение:
//   Массив - ставки НДС
Функция СтавкиПоОперациямОтражаемымВКнигеПродаж(ВерсияПостановленияНДС1137) Экспорт
	
	МассивСтавок = Новый Массив;
	
	МассивСтавок.Добавить(Перечисления.СтавкиНДС.НДС0);
	МассивСтавок.Добавить(Перечисления.СтавкиНДС.НДС10);
	МассивСтавок.Добавить(Перечисления.СтавкиНДС.НДС10_110);
	МассивСтавок.Добавить(Перечисления.СтавкиНДС.НДС18);
	МассивСтавок.Добавить(Перечисления.СтавкиНДС.НДС18_118);
	Если ВерсияПостановленияНДС1137 < 3 Тогда
		МассивСтавок.Добавить(Перечисления.СтавкиНДС.БезНДС);
	КонецЕсли;
	
	Возврат МассивСтавок;
	
КонецФункции
