﻿
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Обработчик события ПриОткрытии формы.
//
Процедура ПриОткрытии()
	
	Объект_ПриОткрытии(ЭлементыФормы);
	
	ИмяТекущейСтраницы = ВосстановитьЗначение("ИмяТекущейСтраницыПодбор");
	Если ТипЗнч(ИмяТекущейСтраницы) = Тип("Строка") Тогда
		Попытка
			ЭлементыФормы.ОбщаяПанель.ТекущаяСтраница = ЭлементыФормы.ОбщаяПанель.Страницы[ИмяТекущейСтраницы];
		Исключение
		КонецПопытки;
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события ПриЗакрытии формы.
//
Процедура ПриЗакрытии()
	
	СохранитьЗначение("ВидОбщегоОбъекта"        , ВидОбщегоОбъекта);
	СохранитьЗначение("ИмяТекущейСтраницыПодбор", ЭлементыФормы.ОбщаяПанель.ТекущаяСтраница.Имя);
	
КонецПроцедуры

// Обработчик события ОбновлениеОтображения формы.
//
Процедура ОбновлениеОтображения()
	
	ЭлементыФормы.ПанельКому.Страницы.Кому.Заголовок         = "Кому (" + Кому.Количество() + ")";
	ЭлементыФормы.ПанельКому.Страницы.Копии.Заголовок        = "Копии (" + Копии.Количество() + ")";
	ЭлементыФормы.ПанельКому.Страницы.СкрытыеКопии.Заголовок = "Скрытые копии (" + СкрытыеКопии.Количество() + ")";
	ЭлементыФормы.ПанельКому.Страницы.Объекты.Заголовок      = "Элементы (" + Объекты.Количество() + ")";
	
КонецПроцедуры

// Обработчик события ОбработкаЗаписиНовогоОбъекта формы.
//
Процедура ОбработкаЗаписиНовогоОбъекта(Объект, Источник)
	
	Объект_ОбработкаЗаписиНовогоОбъекта(Объект, Источник, ЭтаФорма);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ КОМАНДНЫХ ПАНЕЛИ ФОРМЫ

// Обработчик события Нажатие элемента УстановитьОбъектКонтрагентыКонтактныеЛица командных панелей:
//  КоманднаяПанельКонтрагенты
//  КоманднаяПанельФизическиеЛица
//  КоманднаяПанельПрочиеКонтактныеЛица
//  КоманднаяПанельОрганизации.
//
Процедура УстановитьОбъектКонтрагентыКонтактныеЛица(Кнопка)
	
	ВидОбщегоОбъекта = Перечисления.ВидыОбъектовКонтактнойИнформации.Контрагенты;
	Объект_УстановитьОбъект(ЭлементыФормы);
	
КонецПроцедуры

// Обработчик события Нажатие элемента УстановитьОбъектФизическиеЛица командных панелей:
//  КоманднаяПанельКонтрагенты
//  КоманднаяПанельФизическиеЛица
//  КоманднаяПанельПрочиеКонтактныеЛица
//  КоманднаяПанельОрганизации.
//
Процедура УстановитьОбъектФизическиеЛица(Кнопка)
	
	ВидОбщегоОбъекта = Перечисления.ВидыОбъектовКонтактнойИнформации.ФизическиеЛица;
	Объект_УстановитьОбъект(ЭлементыФормы);
	
КонецПроцедуры

// Обработчик события Нажатие элемента УстановитьОбъектПрочиеКонтактныеЛица командных панелей:
//  КоманднаяПанельКонтрагенты
//  КоманднаяПанельФизическиеЛица
//  КоманднаяПанельПрочиеКонтактныеЛица
//  КоманднаяПанельОрганизации.
//
Процедура УстановитьОбъектПрочиеКонтактныеЛица(Кнопка)
	
	ВидОбщегоОбъекта = Перечисления.ВидыОбъектовКонтактнойИнформации.КонтактныеЛица;
	Объект_УстановитьОбъект(ЭлементыФормы);
	
КонецПроцедуры

// Обработчик события Нажатие элемента УстановитьОбъектОрганизации командных панелей:
//  КоманднаяПанельКонтрагенты
//  КоманднаяПанельФизическиеЛица
//  КоманднаяПанельПрочиеКонтактныеЛица
//  КоманднаяПанельОрганизации.
//
Процедура УстановитьОбъектОрганизации(Кнопка)
	
	ВидОбщегоОбъекта = Перечисления.ВидыОбъектовКонтактнойИнформации.Организации;
	Объект_УстановитьОбъект(ЭлементыФормы);
	
КонецПроцедуры

// Обработчик события Нажатие командной панели формы ОсновныеДействияФормы.Выполнить.
//
Процедура КнопкаВыполнитьНажатие(Элемент)
	
	Объект_КнопкаВыполнитьНажатие(Элемент, ЭтаФорма);
	
КонецПроцедуры

// Обработчик события Нажатие командной панели формы КоманднаяПанельРезультатыПоиска.АдресЭлектроннойПочтыВНовыйОбъект.
//
Процедура КоманднаяПанельРезультатыПоискаАдресЭлектроннойПочтыВНовыйОбъект(Кнопка)
	
	СоздатьНовыйОбъектСАдресомЭлектроннойПочты(СтрокаПоиска);
	
КонецПроцедуры

// Обработчик события Нажатие командной панели формы КоманднаяПанельРезультатыПоиска.АдресЭлектроннойПочтыВОбъектТекущейСтроки.
//
Процедура КоманднаяПанельРезультатыПоискаАдресЭлектроннойПочтыВОбъектТекущейСтроки(Кнопка)
	
	Если ЭлементыФормы.РезультатыПоиска.ТекущиеДанные <> Неопределено И ЗначениеЗаполнено(ЭлементыФормы.РезультатыПоиска.ТекущиеДанные.Объект) Тогда
		ДобавитьАдресЭлектроннойПочтыВОбъект(СтрокаПоиска, ЭлементыФормы.РезультатыПоиска.ТекущиеДанные.Объект);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Нажатие командной панели формы КоманднаяПанельГруппыОбъектов.Обновить.
//
Процедура КоманднаяПанельГруппыОбъектовОбновить(Кнопка)
	
	ЗаполнитьСписокГруппРассылки(ГруппыРассылкиСписок);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ТАБЛИЧНЫХ ПОЛЕЙ ФОРМЫ

// Обработчик события ПриАктивизацииСтроки элемента формы КонтрагентыСписок.
//
Процедура КонтрагентыСписокПриАктивизацииСтроки(Элемент)
	
    ПодключитьОбработчикОжидания("ОбработчикОжиданияКонтрагентыСписокПриАктивизацииСтроки", 0.1, Истина);	 	
 
КонецПроцедуры

// Процедура - обработчик ожидания для события ПриАктивизацииСтроки
// табличного поля КонтрагентыСписок
//
Процедура ОбработчикОжиданияКонтрагентыСписокПриАктивизацииСтроки()    
    
    Объект_КонтрагентыСписокПриАктивизацииСтроки(ЭлементыФормы.КонтрагентыСписок, КонтактныеЛицаКонтрагентовСписок);
	ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.КонтактныеЛицаКонтрагентовСписок, ЭлементыФормы.АдресКонтактногоЛицаКонтрагента, СписокДанныхТекущейСтрокиДополнительно);
	ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.КонтрагентыСписок, ЭлементыФормы.АдресКонтрагента, СписокДанныхТекущейСтроки);    
    
КонецПроцедуры // ОбработчикОжиданияКонтрагентыСписокПриАктивизацииСтроки
  
// Обработчик события ПриАктивизацииСтроки элемента формы КонтактныеЛицаКонтрагентовСписок.
//
Процедура КонтактныеЛицаКонтрагентовСписокПриАктивизацииСтроки(Элемент)
	
    ПодключитьОбработчикОжидания("ОбработчикОжиданияКонтактныеЛицаКонтрагентовСписокПриАктивизацииСтроки", 0.1, Истина);	     
    
КонецПроцедуры // КонтактныеЛицаКонтрагентовСписокПриАктивизацииСтроки

// Процедура - обработчик ожидания для события ПриАктивизацииСтроки
// табличного поля КонтактныеЛицаКонтрагентовСписок
//
Процедура ОбработчикОжиданияКонтактныеЛицаКонтрагентовСписокПриАктивизацииСтроки()
    
    ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.КонтактныеЛицаКонтрагентовСписок, ЭлементыФормы.АдресКонтактногоЛицаКонтрагента, СписокДанныхТекущейСтрокиДополнительно);
    
КонецПроцедуры // ОбработчикОжиданияКонтактныеЛицаКонтрагентовСписокПриАктивизацииСтроки

 
// Обработчик события ПриАктивизацииСтроки элемента формы ФизЛицаСписок.
//
Процедура ФизЛицаСписокПриАктивизацииСтроки(Элемент)
	
    ПодключитьОбработчикОжидания("ОбработчикОжиданияФизЛицаСписокПриАктивизацииСтроки", 0.1, Истина);	     
    
КонецПроцедуры

// Процедура - обрабочик ожидания для события ПриАктивизацииСтроки
// табличного поля ФизЛицаСписок
//
Процедура ОбработчикОжиданияФизЛицаСписокПриАктивизацииСтроки()
    
    ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.ФизЛицаСписок, ЭлементыФормы.АдресФизЛица, СписокДанныхТекущейСтроки);
    
КонецПроцедуры // ОбработчикОжиданияФизЛицаСписокПриАктивизацииСтроки

 
// Обработчик события ПриАктивизацииСтроки элемента формы ПрочиеКонтактныеЛицаСписок.
//
Процедура ПрочиеКонтактныеЛицаСписокПриАктивизацииСтроки(Элемент)
	
    ПодключитьОбработчикОжидания("ОбработчикОжиданияПрочиеКонтактныеЛицаСписокПриАктивизацииСтроки", 0.1, Истина);	 
	
КонецПроцедуры // ПрочиеКонтактныеЛицаСписокПриАктивизацииСтроки

// Процедура - обрабочик ожидания для события ПриАктивизацииСтроки
// табличного поля ПрочиеКонтактныеЛицаСписок
//
Процедура ОбработчикОжиданияПрочиеКонтактныеЛицаСписокПриАктивизацииСтроки()
    
    ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.ПрочиеКонтактныеЛицаСписок, ЭлементыФормы.АдресПрочиеКонтактныеЛица, СписокДанныхТекущейСтроки);
    
КонецПроцедуры // ОбработчикОжиданияПрочиеКонтактныеЛицаСписокПриАктивизацииСтроки

 
// Обработчик события ПриАктивизацииСтроки элемента формы ОрганизацииСписок.
//
Процедура ОрганизацииСписокПриАктивизацииСтроки(Элемент)
    
    ПодключитьОбработчикОжидания("ОбработчикОжиданияОрганизацииСписокПриАктивизацииСтроки", 0.1, Истина);	 	
	
КонецПроцедуры

// Процедура - обрабочик ожидания для события ПриАктивизацииСтроки
// табличного поля ОрганизацииСписок
//
Процедура ОбработчикОжиданияОрганизацииСписокПриАктивизацииСтроки()
    
    ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.ОрганизацииСписок, ЭлементыФормы.АдресОрганизации, СписокДанныхТекущейСтроки);
    
КонецПроцедуры // ОбработчикОжиданияОрганизацииСписокПриАктивизацииСтроки

 
// Обработчик события ПриАктивизацииСтроки элемента формы ЛичныеКонтактыСписок.
//
Процедура ЛичныеКонтактыСписокПриАктивизацииСтроки(Элемент)
	
    ПодключитьОбработчикОжидания("ОбработчикОжиданияЛичныеКонтактыСписокПриАктивизацииСтроки", 0.1, Истина);	 
	
КонецПроцедуры

// Процедура - обрабочик ожидания для события ПриАктивизацииСтроки
// табличного поля ЛичныеКонтактыСписок
//
Процедура ОбработчикОжиданияЛичныеКонтактыСписокПриАктивизацииСтроки()
    
    ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.ЛичныеКонтактыСписок, ЭлементыФормы.АдресЛичныеКонтакты, СписокДанныхТекущейСтроки);    
    
КонецПроцедуры // ОбработчикОжиданияЛичныеКонтактыСписокПриАктивизацииСтроки
 
// Обработчик события ПриАктивизацииСтроки элемента формы ПользователиСписок.
//
Процедура ПользователиСписокПриАктивизацииСтроки(Элемент)
    
    ПодключитьОбработчикОжидания("ОбработчикОжиданияПользователиСписокПриАктивизацииСтроки", 0.1, Истина);	 	
	 
КонецПроцедуры // ПользователиСписокПриАктивизацииСтроки

// Процедура - обрабочик ожидания для события ПриАктивизацииСтроки
// табличного поля  ПользователиСписок
//
Процедура ОбработчикОжиданияПользователиСписокПриАктивизацииСтроки()
    
    ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.ПользователиСписок, ЭлементыФормы.АдресПользователи, СписокДанныхТекущейСтроки);
    
КонецПроцедуры // ОбработчикОжиданияПользователиСписокПриАктивизацииСтроки

 
// Обработчик события ПриАктивизацииСтроки элемента формы РезультатыПоиска.
//
Процедура РезультатыПоискаПриАктивизацииСтроки(Элемент)
    
    ПодключитьОбработчикОжидания("ОбработчикОжиданияРезультатыПоискаПриАктивизацииСтроки", 0.1, Истина);	 	
    
КонецПроцедуры // РезультатыПоискаПриАктивизацииСтроки

// Процедура - обрабочик ожидания для события ПриАктивизацииСтроки
// табличного поля  РезультатыПоиска
//
Процедура ОбработчикОжиданияРезультатыПоискаПриАктивизацииСтроки()
    
    Объект_РезультатыПоискаПриАктивизацииСтроки(ЭлементыФормы.РезультатыПоиска, ЭлементыФормы);
    
КонецПроцедуры // ОбработчикОжиданияРезультатыПоискаПриАктивизацииСтроки

 // Обработчик события НачалоВыбораИзСписка элемента формы Кому.АдресЭлектроннойПочты.
//
Процедура КомуАдресЭлектроннойПочтыНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка)
	
	Объект_АдресЭлектроннойПочтыНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка, ЭлементыФормы.Кому, ЭтаФорма);
	
КонецПроцедуры

// Обработчик события ПриИзменении элемента формы Кому.Объект.
//
Процедура КомуОбъектПриИзменении(Элемент)
	
	Объект_ОбъектЭлектроннойПочтыПриИзменении(Элемент, ЭлементыФормы.Кому);
	
КонецПроцедуры

// Обработчик события НачалоВыбораИзСписка элемента формы Копии.АдресЭлектроннойПочты.
//
Процедура КопииАдресЭлектроннойПочтыНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка)
	
	Объект_АдресЭлектроннойПочтыНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка, ЭлементыФормы.Копии, ЭтаФорма);
	
КонецПроцедуры

// Обработчик события ПриИзменении элемента формы Копии.Объект.
//
Процедура КопииОбъектПриИзменении(Элемент)
	
	Объект_ОбъектЭлектроннойПочтыПриИзменении(Элемент, ЭлементыФормы.Копии);
	
КонецПроцедуры

// Обработчик события НачалоВыбораИзСписка элемента формы СкрытыеКопии.АдресЭлектроннойПочты.
//
Процедура СкрытыеКопииАдресЭлектроннойПочтыНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка)
	
	Объект_АдресЭлектроннойПочтыНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка, ЭлементыФормы.СкрытыеКопии, ЭтаФорма);
	
КонецПроцедуры

// Обработчик события ПриИзменении элемента формы СкрытыеКопии.Объект.
//
Процедура СкрытыеКопииОбъектПриИзменении(Элемент)
	
	Объект_ОбъектЭлектроннойПочтыПриИзменении(Элемент, ЭлементыФормы.СкрытыеКопии);
	
КонецПроцедуры


// Обработчик события ПередНачаломИзменения элемента формы Объекты.
//
Процедура ОбъектыПередНачаломИзменения(Элемент, Отказ)
	
	Отказ = Истина;
	
	Если Элемент.ТекущиеДанные <> Неопределено И ЗначениеЗаполнено(Элемент.ТекущиеДанные.Объект) Тогда
		ОткрытьФормуОбъекта(Элемент.ТекущиеДанные.Объект);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события ПередНачаломИзменения элемента формы РезультатыПоиска.
//
Процедура РезультатыПоискаПередНачаломИзменения(Элемент, Отказ)
	
	Отказ = Истина;
	
	Если Элемент.ТекущиеДанные <> Неопределено И ЗначениеЗаполнено(Элемент.ТекущиеДанные.Объект) Тогда
		ОткрытьФормуОбъекта(Элемент.ТекущиеДанные.Объект);
	КонецЕсли; 
	
КонецПроцедуры


// Обработчик события Выбор элемента формы КонтрагентыСписок.
//
Процедура КонтрагентыСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если НЕ (Элемент.ТекущиеДанные <> Неопределено И Элемент.ТекущиеДанные.ЭтоГруппа) Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.КонтрагентыСписок, ЭлементыФормы.АдресКонтрагента, СтандартнаяОбработка, "Контрагенты");
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы КонтактныеЛицаКонтрагентовСписок.
//
Процедура КонтактныеЛицаКонтрагентовСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если Элемент.ТекущиеДанные <> Неопределено Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтрокиДополнительно, ЭлементыФормы.КонтактныеЛицаКонтрагентовСписок, ЭлементыФормы.АдресКонтактногоЛицаКонтрагента, СтандартнаяОбработка, "КонтактныеЛицаКонтрагентов");
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы ФизЛицаСписок.
//
Процедура ФизЛицаСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если НЕ (Элемент.ТекущиеДанные <> Неопределено И Элемент.ТекущиеДанные.ЭтоГруппа) Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ФизЛицаСписок, ЭлементыФормы.АдресФизЛица, СтандартнаяОбработка, "ФизическиеЛица");
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы ПрочиеКонтактныеЛицаСписок.
//
Процедура ПрочиеКонтактныеЛицаСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если Элемент.ТекущиеДанные <> Неопределено Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ПрочиеКонтактныеЛицаСписок, ЭлементыФормы.АдресПрочиеКонтактныеЛица, СтандартнаяОбработка, "КонтактныеЛица");
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы ОрганизацииСписок.
//
Процедура ОрганизацииСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если Элемент.ТекущиеДанные <> Неопределено Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ОрганизацииСписок, ЭлементыФормы.АдресОрганизации, СтандартнаяОбработка, "Организации");
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы ЛичныеКонтактыСписок.
//
Процедура ЛичныеКонтактыСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если Элемент.ТекущиеДанные <> Неопределено Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ЛичныеКонтактыСписок, ЭлементыФормы.АдресЛичныеКонтакты, СтандартнаяОбработка, "ЛичныеКонтакты");
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы ПользователиСписок.
//
Процедура ПользователиСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если НЕ (Элемент.ТекущиеДанные <> Неопределено И Элемент.ТекущиеДанные.ЭтоГруппа) Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ПользователиСписок, ЭлементыФормы.АдресПользователи, СтандартнаяОбработка, "Пользователи");
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы РезультатыПоиска.
//
Процедура РезультатыПоискаВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если НЕ (Элемент.ТекущиеДанные <> Неопределено И Элемент.ТекущиеДанные.Объект.ЭтоГруппа) Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементыРезультатовПоиска(ЭлементыФормы, РезультатыПоиска);
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Выбор элемента формы ГруппыРассылкиСписок.
//
Процедура ГруппыРассылкиСписокВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если Элемент.ТекущиеДанные <> Неопределено Тогда
		Если НЕ (ОткрытаДляВыбора И ОткрытиеПриВыборе) Тогда
			СтандартнаяОбработка = Ложь;
			ДобавитьЭлементыГруппРассылки(ЭлементыФормы, ГруппыРассылкиСписок);
			ОбработкаВыбораТабличныхПолей(ЭтаФорма);
		КонецЕсли; 
	КонецЕсли; 
	
КонецПроцедуры


// Обработчик события ПередУдалением элемента формы РезультатыПоиска.
//
Процедура РезультатыПоискаПередУдалением(Элемент, Отказ)
	
	Отказ = Истина;
	
КонецПроцедуры

// Обработчик события ПередНачаломДобавления элемента формы РезультатыПоиска.
//
Процедура РезультатыПоискаПередНачаломДобавления(Элемент, Отказ, Копирование)
	
	Отказ = Истина;
	
КонецПроцедуры

// Обработчик события ПриВыводеСтроки элемента формы ГруппыРассылкиСписок.
//
Процедура ГруппыРассылкиСписокПриВыводеСтроки(Элемент, ОформлениеСтроки, ДанныеСтроки)
	
	Объект_ГруппыРассылкиСписокПриВыводеСтроки(Элемент, ОформлениеСтроки, ДанныеСтроки);
	
КонецПроцедуры

// Обработчик события ПередНачаломДобавления элемента формы ГруппыРассылкиСписок.
//
Процедура ГруппыРассылкиСписокПередНачаломДобавления(Элемент, Отказ, Копирование)
	
	Объект_ГруппыРассылкиСписокПередНачаломДобавления(Элемент, Отказ, Копирование, ЭтаФорма);
	
КонецПроцедуры

// Обработчик события ПередНачаломИзменения элемента формы ГруппыРассылкиСписок.
//
Процедура ГруппыРассылкиСписокПередНачаломИзменения(Элемент, Отказ)
	
	Объект_ГруппыРассылкиСписокПередНачаломИзменения(Элемент, Отказ, ЭтаФорма);
	
КонецПроцедуры

// Обработчик события ПередУдалением элемента формы ГруппыРассылкиСписок.
//
Процедура ГруппыРассылкиСписокПередУдалением(Элемент, Отказ)
	
	Объект_ГруппыРассылкиСписокПередУдалением(Элемент, Отказ);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ КНОПОК ФОРМЫ

// Обработчик события Нажатие элемента формы КнопкаДобавитьКонтрагенты.
//
Процедура КнопкаДобавитьКонтрагентыНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.КонтрагентыСписок, ЭлементыФормы.АдресКонтрагента, , "Контрагенты");
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьКонтрагентыВсе.
//
Процедура КнопкаДобавитьКонтрагентыВсеНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.КонтрагентыСписок, ЭлементыФормы.АдресКонтрагента, , "Контрагенты", Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьКонтрагентыВсе.
//
Процедура КнопкаДобавитьКонтактныеЛицаКонтрагентовНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтрокиДополнительно, ЭлементыФормы.КонтактныеЛицаКонтрагентовСписок, ЭлементыФормы.АдресКонтактногоЛицаКонтрагента, , "КонтактныеЛица");
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьКонтактныеЛицаКонтрагентовВсе.
//
Процедура КнопкаДобавитьКонтактныеЛицаКонтрагентовВсеНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтрокиДополнительно, ЭлементыФормы.КонтактныеЛицаКонтрагентовСписок, ЭлементыФормы.АдресКонтактногоЛицаКонтрагента, , "КонтактныеЛица", Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьФизЛица.
//
Процедура КнопкаДобавитьФизЛицаНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ФизЛицаСписок, ЭлементыФормы.АдресФизЛица, , "ФизическиеЛица");
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьФизЛицаВсе.
//
Процедура КнопкаДобавитьФизЛицаВсеНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ФизЛицаСписок, ЭлементыФормы.АдресФизЛица, , "ФизическиеЛица", Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьПрочиеКонтактныеЛица.
//
Процедура КнопкаДобавитьПрочиеКонтактныеЛицаНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ПрочиеКонтактныеЛицаСписок, ЭлементыФормы.АдресПрочиеКонтактныеЛица, , "ПрочиеКонтактныеЛица");
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьПрочиеКонтактныеЛицаВсе.
//
Процедура КнопкаДобавитьПрочиеКонтактныеЛицаВсеНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ПрочиеКонтактныеЛицаСписок, ЭлементыФормы.АдресПрочиеКонтактныеЛица, , "ПрочиеКонтактныеЛица", Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьОрганизации.
//
Процедура КнопкаДобавитьОрганизацииНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ОрганизацииСписок, ЭлементыФормы.АдресОрганизации, , "Организации");
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьОрганизацииВсе.
//
Процедура КнопкаДобавитьОрганизацииВсеНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ОрганизацииСписок, ЭлементыФормы.АдресОрганизации, , "Организации", Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьЛичныеКонтакты.
//
Процедура КнопкаДобавитьЛичныеКонтактыНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ЛичныеКонтактыСписок, ЭлементыФормы.АдресЛичныеКонтакты, , "ЛичныеКонтакты");
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьЛичныеКонтактыВсе.
//
Процедура КнопкаДобавитьЛичныеКонтактыВсеНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ЛичныеКонтактыСписок, ЭлементыФормы.АдресЛичныеКонтакты, , "ЛичныеКонтакты", Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьПользователи.
//
Процедура КнопкаДобавитьПользователиНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ПользователиСписок, ЭлементыФормы.АдресПользователи, , "Пользователи");
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьПользователиВсе.
//
Процедура КнопкаДобавитьПользователиВсеНажатие(Элемент)
	
	ДобавитьЭлементы(ЭлементыФормы, СписокДанныхТекущейСтроки, ЭлементыФормы.ПользователиСписок, ЭлементыФормы.АдресПользователи, , "Пользователи", Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьГруппыОбъектов.
//
Процедура КнопкаДобавитьГруппыОбъектовНажатие(Элемент)
	
	ДобавитьЭлементыГруппРассылки(ЭлементыФормы, ГруппыРассылкиСписок);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьГруппыОбъектовВсе.
//
Процедура КнопкаДобавитьГруппыОбъектовВсеНажатие(Элемент)
	
	ДобавитьЭлементыГруппРассылки(ЭлементыФормы, ГруппыРассылкиСписок, Истина);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьПоиск.
//
Процедура КнопкаДобавитьПоискНажатие(Элемент)
	
	ДобавитьЭлементыРезультатовПоиска(ЭлементыФормы, РезультатыПоиска);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьПоискВсе.
//
Процедура КнопкаДобавитьПоискВсеНажатие(Элемент)
	
	ДобавитьЭлементыРезультатовПоиска(ЭлементыФормы, РезультатыПоиска, Истина);
	
КонецПроцедуры


// Обработчик события Нажатие элемента формы КнопкаУдалитьВсе.
//
Процедура КнопкаУдалитьВсеНажатие(Элемент)
	
	ОтветНаВопрос = Вопрос("Очистить информацию на закладках ""Кому"", ""Копии"" и ""Скрытые копии"" ?", РежимДиалогаВопрос.ДаНет);
	Если ОтветНаВопрос <> КодВозвратаДиалога.Да  Тогда
		Возврат;
	КонецЕсли;
	
	Кому.Очистить();
	Копии.Очистить();
	СкрытыеКопии.Очистить();
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьКому.
//
Процедура КнопкаУдалитьКомуНажатие(Элемент)
	
	Если ЭлементыФормы.Кому.ТекущиеДанные <> Неопределено Тогда
		Кому.Удалить(ЭлементыФормы.Кому.ТекущиеДанные);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьКомуВсе.
//
Процедура КнопкаУдалитьКомуВсеНажатие(Элемент)
	
	ОтветНаВопрос = Вопрос("Очистить информацию на закладке ""Кому"" ?", РежимДиалогаВопрос.ДаНет);
	Если ОтветНаВопрос <> КодВозвратаДиалога.Да  Тогда
		Возврат;
	КонецЕсли;
	
	Кому.Очистить();
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьКопии.
//
Процедура КнопкаУдалитьКопииНажатие(Элемент)
	
	Если ЭлементыФормы.Копии.ТекущиеДанные <> Неопределено Тогда
		Копии.Удалить(ЭлементыФормы.Копии.ТекущиеДанные);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьКопииВсе.
//
Процедура КнопкаУдалитьКопииВсеНажатие(Элемент)
	
	ОтветНаВопрос = Вопрос("Очистить информацию на закладке ""Копии"" ?", РежимДиалогаВопрос.ДаНет);
	Если ОтветНаВопрос <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	Копии.Очистить();
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьСкрытыеКопии.
//
Процедура КнопкаУдалитьСкрытыеКопииНажатие(Элемент)
	
	Если ЭлементыФормы.СкрытыеКопии.ТекущиеДанные <> Неопределено Тогда
		СкрытыеКопии.Удалить(ЭлементыФормы.СкрытыеКопии.ТекущиеДанные);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьСкрытыеКопииВсе.
//
Процедура КнопкаУдалитьСкрытыеКопииВсеНажатие(Элемент)
	
	ОтветНаВопрос = Вопрос("Очистить информацию на закладке ""Скрытые копии"" ?", РежимДиалогаВопрос.ДаНет);
	Если ОтветНаВопрос <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	СкрытыеКопии.Очистить();
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьОбъекты.
//
Процедура КнопкаУдалитьОбъектыНажатие(Элемент)
	
	Если ЭлементыФормы.Объекты.ТекущиеДанные <> Неопределено Тогда
		Объекты.Удалить(ЭлементыФормы.Объекты.ТекущиеДанные);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаУдалитьОбъектыВсе.
//
Процедура КнопкаУдалитьОбъектыВсеНажатие(Элемент)
	
	
	ОтветНаВопрос = Вопрос("Очистить информацию на закладке ""Объекты"" ?", РежимДиалогаВопрос.ДаНет);
	Если ОтветНаВопрос <> КодВозвратаДиалога.Да  Тогда
		Возврат;
	КонецЕсли;
	
	Объекты.Очистить();
	
КонецПроцедуры

// Обработчик события ПриИзменении элемента формы СтрокаПоиска.
//
Процедура СтрокаПоискаПриИзменении(Элемент)
	
	Объект_НайтиНажатие(Элемент, РезультатыПоиска, Элемент.Значение);
	
КонецПроцедуры

// Обработчик события ПриАктивизацииСтроки элемента формы ГруппыРассылкиСписок.
//
Процедура ГруппыРассылкиСписокПриАктивизацииСтроки(Элемент)
	
	Объект_ГруппыРассылкиСписокПриАктивизацииСтроки(Элемент, СоставГруппыРассылки);
	
КонецПроцедуры

// Обработчик события ПриАктивизацииСтроки элемента формы СоставГруппыРассылки.
//
Процедура СоставГруппыРассылкиПриАктивизацииСтроки(Элемент)
	
    ПодключитьОбработчикОжидания("ОбработчикОжиданияСоставГруппыРассылкиПриАктивизацииСтроки", 0.1, Истина);	 
    
КонецПроцедуры // СоставГруппыРассылкиПриАктивизацииСтроки

// Процедура - обрабочик ожидания для события ПриАктивизацииСтроки
// табличного поля СоставГруппыРассылки
//
Процедура ОбработчикОжиданияСоставГруппыРассылкиПриАктивизацииСтроки()
    
    ПолучитьАдресЭлектроннойПочты(ЭлементыФормы.СоставГруппыРассылки, ЭлементыФормы.АдресЭлементГруппыРассылки, СписокДанныхТекущейСтрокиДополнительно, "ОбъектРассылки");
    
КонецПроцедуры // ОбработчикОжиданияСоставГруппыРассылкиПриАктивизацииСтроки

 
// Обработчик события Выбор элемента формы СоставГруппыРассылки.
//
Процедура СоставГруппыРассылкиВыбор(Элемент, ВыбраннаяСтрока, Колонка, СтандартнаяОбработка)
	
	Если НЕ (Элемент.ТекущиеДанные <> Неопределено И Элемент.ТекущиеДанные.ОбъектРассылки.ЭтоГруппа) Тогда
		СтандартнаяОбработка = Ложь;
		ДобавитьЭлементыГруппыРассылки(ЭлементыФормы, Элемент, СписокДанныхТекущейСтрокиДополнительно, ЭлементыФормы.АдресЭлементГруппыРассылки);
		ОбработкаВыбораТабличныхПолей(ЭтаФорма);
	КонецЕсли; 
	
КонецПроцедуры

// Обработчик события ПередНачаломИзменения элемента формы СоставГруппыРассылки.
//
Процедура СоставГруппыРассылкиПередНачаломИзменения(Элемент, Отказ)
	
	Объект_СоставГруппыРассылкиПередНачаломИзменения(Элемент, Отказ);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьЭлементГруппыОбъектовВсе.
//
Процедура КнопкаДобавитьЭлементГруппыОбъектовВсеНажатие(Элемент)
	
	ДобавитьЭлементыГруппРассылки(ЭлементыФормы, ГруппыРассылкиСписок);
	
КонецПроцедуры

// Обработчик события Нажатие элемента формы КнопкаДобавитьЭлементГруппыОбъектов.
//
Процедура КнопкаДобавитьЭлементГруппыОбъектовНажатие(Элемент)
	
	ДобавитьЭлементыГруппыРассылки(ЭлементыФормы, ЭлементыФормы.СоставГруппыРассылки, СписокДанныхТекущейСтрокиДополнительно, ЭлементыФормы.АдресЭлементГруппыРассылки);
	
КонецПроцедуры

Процедура КнопкаНайтиНажатие(Элемент)
	
	Объект_НайтиНажатие(ЭлементыФормы.СтрокаПоиска, РезультатыПоиска, ЭлементыФормы.СтрокаПоиска.Значение);
	
КонецПроцедуры
